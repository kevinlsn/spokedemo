import placesData from "./data.json";

export const getDataFromServer = async ({ type = "", placeIndex = 0 }) => {
  try {
    const items = {
      cars: [
        {
          id: "1234567890",
          position: placesData.places2[placeIndex].geometry,
        },
      ],
    };

    return { data: items, success: true };
  } catch (error) {
    return { data: "Error", success: false };
  }
};

export const getAlertsFromServer = async () => {
  try {
    const items = {
      alerts: [
        {
          id: "1234567890",
          type: "collision",
          value: "right",
          time: "1 second",
        },
      ],
      warnings: [],
    };

    return { data: items, success: true };
  } catch (error) {
    return { data: "Error", success: false };
  }
};
