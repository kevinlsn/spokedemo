import React, { useState, useEffect } from "react";
import { MapContainer, TileLayer } from "react-leaflet";

import { getAlertsFromServer, getDataFromServer } from "../service/api";
import placesData from "../service/data.json";

import CarMarker from "./Markers/CarMarker";
import BycicleMarker from "./Markers/BycycleMarker";
import Popup from "./Alerts/Popup";
import Button from "./Button/Button";

import "../assets/alert.png";
import "leaflet/dist/leaflet.css";
import AlertImg from "../assets/alert.png";

const { places = [] } = placesData;

const MapView = () => {
  const [mapState, setMapState] = useState({
    currentLocation: { lat: 18.996458, lng: -98.203182 },
    zoom: 16,
  });

  // Simulate bycicle route

  const [bycycleIndex, setBycicleIndex] = useState(0);

  useEffect(() => {
    if (bycycleIndex >= places.length - 1) setBycicleIndex(0);

    const timeoutId = setTimeout(async () => {
      setBycicleIndex(bycycleIndex + 1);
    }, 1000);

    return () => clearTimeout(timeoutId);
  }, [bycycleIndex]);

  // Simulate cars data from server

  const [data, setData] = useState({
    cars: [],
  });

  useEffect(() => {
    let placeIndex = 0;
    const intervalId = setInterval(async () => {
      const { data, success } = await getDataFromServer({ placeIndex });
      if (success) setData(data);

      placeIndex >= places.length ? (placeIndex = 0) : placeIndex++;
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  // Scripted alert

  const [showAlert, setShowAlert] = useState(false);
  useEffect(() => {
    setTimeout(async () => {
      const { data, success } = await getAlertsFromServer();
      if (success) setShowAlert(true);
    }, 10000);
  }, []);

  //  if (is_ok) {
  //     const currentLocation = {
  //       lat: location.state.latitude,
  //       lng: location.state.longitude,
  //     };
  //     console.log(state);
  //     setState({
  //       ...state,
  //       /* data: {
  //         places: state.data.places.concat({
  //           name: "new",
  //           geometry: [currentLocation.lat, currentLocation.lng],
  //         }),
  //       },
  //       currentLocation,
  //     });
  //     navigate("/map");
  //   }

  return (
    <div>
      <MapContainer center={mapState.currentLocation} zoom={mapState.zoom}>
        <TileLayer
          url="https://api.maptiler.com/maps/streets/256/{z}/{x}/{y}.png?key=JwlI05ut1fsEtcnSrgWb"
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        />
        {/* <Button /> */}

        <BycicleMarker coords={places[bycycleIndex].geometry} />

        {data.cars.map(({ position, id }) => (
          <CarMarker key={id} coords={position} />
        ))}

        <Popup show={showAlert} onClose={() => setShowAlert(false)}>
          <div className="imagePopup">
            <img alt="" src={AlertImg} />
          </div>
          <div className="alertPopup">
            <h3 className="texth3"> ¡ALERT! </h3>
            <h2 className="texth2"> Collision in less than 5 seconds!! </h2>
          </div>
        </Popup>
      </MapContainer>
    </div>
  );
};

export default MapView;
