import React from "react";
import "./Popup.css";

function Popup({ show, onClose, children }) {
  return show ? (
    <div className="popup">
      <div className="popup-inner">
        <button className="close-btn" onClick={onClose}>
          X
        </button>
        {children}
      </div>
    </div>
  ) : (
    ""
  );
}

export default Popup;
