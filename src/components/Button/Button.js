import React from "react";
import "./Button.css";

const Button = ({ text = "Repeat" }) => {
  return <button className="button">{text}</button>;
};

export default Button;
