import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "../Home.css";

const Home = () => {
  const [state, setState] = useState({
    longitude: 0,
    latitude: 0,
  });

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        // console.log(position);
        setState({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
        });
      },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      },
      {
        enableHighAccuracy: true,
      }
    );
  }, []);

  return (
    <div>
      <div className="text-container">
        <h1 className="title"> Spoke Demo </h1>
        <h2 className="subtitle">
          See your location on the map, click and navigate...
        </h2>
        <h3 className="actualLocation"> Your actual location is ...</h3>
        <p className="latitude"> Latitude: {state.latitude} </p>
        <p className="longitude"> Longitude: {state.longitude} </p>
      </div>
      <div className="buttoncontainer">
        <Link to="/map" state={state} className="button">
          See marker
        </Link>
      </div>
    </div>
  );
};

export default Home;
