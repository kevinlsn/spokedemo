import React from "react";
import { Marker } from "react-leaflet";
import MarkerPopup from "./MarkerPopup";
import L from "leaflet";

export const customBycicleIcon = L.icon({
  iconUrl: require("../../assets/bycicle.png"),
  iconAnchor: [5, 5],
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [24, 24],
  className: "leaflet-venue-icon",
});

const BycicleMarker = ({ coords }) => {
  return !coords ? (
    <></>
  ) : (
    <Marker position={coords} icon={customBycicleIcon}>
      <MarkerPopup data={{ name: "test" }} />
    </Marker>
  );
};

export default BycicleMarker;
