import React from "react";
import { Marker } from "react-leaflet";
import MarkerPopup from "./MarkerPopup";
import L from "leaflet";

export const customCarIcon = L.icon({
  iconUrl: require("../../assets/marker.png"),
  iconAnchor: [5, 5],
  shadowUrl: require("../../assets/marker.png"),
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [0, 0],
  className: "leaflet-venue-icon",
});

const CarMarker = ({ coords }) => {
  return !coords ? (
    <></>
  ) : (
    <Marker position={coords} icon={customCarIcon}>
      <MarkerPopup data={{ name: "test" }} />
    </Marker>
  );
};

export default CarMarker;
